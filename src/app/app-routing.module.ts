import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NotFoundComponent } from './system/not-found/not-found.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'check-list',
    pathMatch: 'full'
  },
  {
    path: 'check-list',
    loadChildren: () => import('./check-list/check-list.module').then((m) => m.CheckListModule),
    //canActivate: AuthGuard
  },
  {
    path: '**',
    component: NotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
