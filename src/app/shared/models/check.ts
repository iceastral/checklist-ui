export interface ICheck {
    id: number;
    description: string;
    isChecked: boolean;
}