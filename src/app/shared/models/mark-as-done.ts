export interface IMarkAsDoneRequest {
    teamMemberId: number;
    checkId: number;
}