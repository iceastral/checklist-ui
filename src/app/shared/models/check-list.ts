import { ICheck } from "./check";

export interface ICheckList {
    teamMemberId: number;
    checks: Array<ICheck>
}

