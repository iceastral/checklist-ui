export interface IMemberResult {
    id: number;
    name: string;
    checkListDone: boolean;
}