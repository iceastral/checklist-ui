import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CheckListComponent } from "./components/check-list/check-list.component";
import { DashboardComponent } from "./components/dashboard/dashboard.component";

const routes: Routes = [
    {
        path: '',
        children: [
            {
                path: ':id/:name',
                component: CheckListComponent
            },
            {
                path: '',
                component: DashboardComponent
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [RouterModule]
})
export class CheckListRoutingModule {}