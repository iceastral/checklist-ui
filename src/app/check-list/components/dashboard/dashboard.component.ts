import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { CheckListService } from 'src/app/core/services/check-list.service';
import { IMemberResult } from 'src/app/shared/models/member-result';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent {
  users$: Observable<Array<IMemberResult>>;
  constructor(
    private router: Router,
    private checkListService: CheckListService
  ){
    this.users$ = this.checkListService.getTeamResults()
  }

  goToCheckList(id: number, name: string) {
    this.router.navigate([`/check-list/${id}/${name}`])
  }

}
