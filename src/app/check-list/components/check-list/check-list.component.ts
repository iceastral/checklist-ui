import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {  Observable, switchMap } from 'rxjs';
import { CheckListService } from 'src/app/core/services/check-list.service';
import { ICheck } from 'src/app/shared/models/check';
import { ICheckList } from 'src/app/shared/models/check-list';

@Component({
  selector: 'app-check-list',
  templateUrl: './check-list.component.html',
  styleUrls: ['./check-list.component.scss']
})
export class CheckListComponent {
  teamMemberCheckList$: Observable<ICheckList>;
  userId: number;
  userName: string;

  constructor(
    private activatedRoute: ActivatedRoute,
    private checkListService: CheckListService
  ) {
    this.userId = Number(this.activatedRoute.snapshot.params['id']);
    this.userName = this.activatedRoute.snapshot.params['name'];
    this.teamMemberCheckList$ = this.checkListService.getTeamMemberCheckList(this.userId);
  }

  markThisCheck(checkId: number): void {
    this.teamMemberCheckList$ = this.checkListService.markAsDone({
      teamMemberId: this.userId,
      checkId
    }).pipe(
      switchMap(() => 
        this.checkListService.getTeamMemberCheckList(this.userId)
      )
    );
  }

  checkListDone(checks: Array<ICheck>): boolean {
    return checks.every(c => c.isChecked);
  }

}
