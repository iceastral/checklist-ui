import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CheckListComponent } from './components/check-list/check-list.component';
import { CheckListRoutingModule } from './check-list-routing.module';
import { DashboardComponent } from './components/dashboard/dashboard.component';



@NgModule({
  declarations: [
    CheckListComponent,
    DashboardComponent
  ],
  imports: [
    CommonModule,
    CheckListRoutingModule
  ]
})
export class CheckListModule { }