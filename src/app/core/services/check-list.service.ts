import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ICheckList } from "src/app/shared/models/check-list";
import { IMarkAsDoneRequest } from "src/app/shared/models/mark-as-done";
import { IMemberResult } from "src/app/shared/models/member-result";
import { environment } from 'src/environments/environments';

@Injectable({ providedIn: 'root' })
export class CheckListService {
    private url: string;

    constructor(private http: HttpClient) {
        this.url = `${environment.apiUrl}/CheckList`;
    }

    getTeamResults(): Observable<Array<IMemberResult>> {
        return this.http.get<Array<IMemberResult>>(this.url);
    }

    getTeamMemberCheckList(memberId: number): Observable<ICheckList> {
        return this.http.get<ICheckList>(`${this.url}/${memberId}`)
    }

    markAsDone(request: IMarkAsDoneRequest) {
        return this.http.post<boolean>(`${this.url}/MarkAsDone`, request);
    }
}